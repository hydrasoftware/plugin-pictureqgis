from qgis.PyQt.QtWidgets import QApplication
from . import PictureWidget
import sys


app = QApplication(sys.argv)
pw = PictureWidget(config={'extension': '.mp4'})
pw.filename.setText(r'C:\Users\vincent.mora\Pictures\test.mp4')
#pw.setEnabled(False)
pw.show()
app.exec_()

pw = PictureWidget(config={'extension': '.jpg'})
pw.filename.setText(r'C:\Users\vincent.mora\Pictures\test.jpg')
#pw.setEnabled(False)
pw.show()
app.exec_()