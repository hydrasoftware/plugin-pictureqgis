import os
import win32gui, win32con
import time
from glob import glob

from qgis.gui import QgsEditorWidgetFactory, QgsEditorWidgetWrapper, QgsEditorConfigWidget
from qgis.PyQt.QtWidgets import QWidget, QLineEdit, QPushButton, QHBoxLayout, QErrorMessage, QLabel, QVBoxLayout, QSpinBox, QComboBox
from qgis.PyQt.QtGui import QPixmap, QPainter
from qgis.PyQt.QtCore import QSize, QObject, QEvent, Qt, QUrl
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from qgis.gui import QgsGui

class Snapshotter:
    """Takes a picture with the windows camera app
    if the camera is not running, launch it, otherwise maximize it and raise it to foreground
    the object constructor returns once the picuture is taken or the camera application closed (cancel)
    do not keep a referenc on the instance.
    """
    
    CAMERA_WINDOW_HANDLE = None
    CAMERA_WINDOW_RECT = None
    CAMERA_ROLL = os.path.join(os.path.expanduser('~'), 'Pictures', 'Camera Roll')
    CAMERA_NAMES = ('Caméra', 'Camera')
    
    def __init__(self, filename):
        assert(filename.endswith('.jpg') or filename.endswith('.mp4'))
        pattern = os.path.join(self.CAMERA_ROLL, '*'+filename[-4:])
        if not self.__find_camera():
            os.system('start microsoft.windows.camera:')
            self.__find_camera()
        
        win32gui.ShowWindow(self.CAMERA_WINDOW_HANDLE, win32con.SW_MAXIMIZE)
        win32gui.SetForegroundWindow(self.CAMERA_WINDOW_HANDLE)

        # wait for picture to be taken
        files = set(glob(pattern))
        sz = -1
        while True:
            time.sleep(.5)
            if not self.__find_camera():
                return # aborted
            new_files = list(set(glob(pattern)).difference(files))
            if len(new_files):
                new_sz = os.stat(new_files[0]).st_size
                if new_sz == sz:
                    if os.path.exists(filename):
                        os.remove(filename)
                    os.rename(new_files[0], filename)
                    for f in new_files[1:]: # just in casse several photos are taken at once
                        os.remove(f)
                    win32gui.ShowWindow(self.CAMERA_WINDOW_HANDLE, win32con.SW_MINIMIZE)
                    return
                sz = new_sz if new_sz != 0 else -1 
                
    def __find_camera(self):
        self.CAMERA_WINDOW_HANDLE = None
        self.CAMERA_WINDOW_RECT = None
        win32gui.EnumWindows(self.__find_camera_window_handle,0)        
        return self.CAMERA_WINDOW_HANDLE is not None
       
    def __find_camera_window_handle(self, hwnd, ctx):
        if win32gui.GetWindowText(hwnd) in self.CAMERA_NAMES and win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE) == 2097408:
            self.CAMERA_WINDOW_RECT = win32gui.GetWindowRect(hwnd)
            self.CAMERA_WINDOW_HANDLE = hwnd

class Filter(QObject):
    """allows display of full color picture when widget is disabled"""
    def __init__(self, parent=None):
        super().__init__(parent=parent)

    def eventFilter(self, label, event):
        if event.type() == QEvent.Paint:
            painter = QPainter(label)
            if label.pixmap():
                pixmap = label.pixmap().scaled(label.size(), Qt.KeepAspectRatio)    
                label.style().drawItemPixmap(painter, label.rect(), Qt.AlignHCenter | Qt.AlignVCenter, pixmap)
            return True
        return False

class PictureWidget(QWidget):

    def __init__(self, config=None, parent=None):
        super().__init__(parent=parent)
        self.ext = config['extension'] if config is not None and 'extension' in config and config['extension'] is not None else '.jpg'
        self.sz = QSize(
            int(config['width']) if config is not None and 'width' in config and config['width'] is not None else 400, 
            int(config['height']) if config is not None and 'height' in config and config['height'] is not None else 300
            )
        
        self.button = QPushButton('📷')
        self.button.setMaximumWidth(60)
        self.filename = QLineEdit()
        hlayout = QHBoxLayout()
        hlayout.addWidget(self.button)
        hlayout.addWidget(self.filename)
        
        self.f = Filter()

        if self.ext == '.jpg':
            self.label = QLabel('hello')
            self.label.installEventFilter(self.f)
        else:
            self.label = QVideoWidget()
            self.label.setMinimumHeight(self.sz.height())
            self.player = None


        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addLayout(hlayout)
        self.setLayout(layout)

        self.button.clicked.connect(self.snapshot)
        self.filename.textChanged.connect(self.display_or_preview)
        self.display_or_preview()
       
    def snapshot(self):
        if self.filename.text().endswith(self.ext):
            if self.ext == '.jpg':
                self.label.removeEventFilter(self.f) # otherwise the file is locked
                Snapshotter(self.filename.text())
                self.label.installEventFilter(self.f)
            else:
                self.player and self.player.setParent(None)
                self.player = None
                Snapshotter(self.filename.text())

            self.display_or_preview()

    def display_or_preview(self, dummy=None):
        self.filename.setStyleSheet('color: black;' if self.filename.text().endswith(self.ext) else 'color: red;')

        if os.path.exists(self.filename.text()) and self.filename.text().endswith(self.ext):
            if self.ext == '.jpg':
                px = QPixmap(self.sz)
                px.load(self.filename.text())
                px = px.scaled(self.sz, Qt.KeepAspectRatio)
                self.label.setPixmap(px)
            else:
                self.player = QMediaPlayer(self)
                self.player.setMedia(QMediaContent(QUrl.fromLocalFile(self.filename.text())))
                self.player.setVideoOutput(self.label)
                self.player.play()
        else:
            if self.ext == '.jpg':
                px = QPixmap(self.sz)
                self.label.setPixmap(px)
            else:
                self.player and self.player.setParent(None)
                self.player = None
            


class PictureWidgetWrapper(QgsEditorWidgetWrapper):
    
    def __init__(self, vl, fieldIdx, editor, parent):
        super().__init__(vl, fieldIdx, editor, parent)

    def createWidget(self, parent):
        return PictureWidget(self.config())

    def initWidget(self, editor):
        self.__pw = editor
        self.__pw.filename.textChanged.connect(self.emitValueChanged)

    def valid(self):
        return True

    def updateValues(self, value, lst=None):
        self.__pw.filename.setText(value)

    def setValue(self, value):
        self.updateValues(value)

    def value(self):
        return self.__pw.filename.text()


class PictureWidgetConfig(QgsEditorConfigWidget):

    def __init__(self, vl, fieldIdx, parent):
        super().__init__(vl, fieldIdx, parent)
        self.__cfg = {}
        self.display_width = QSpinBox()
        self.display_width.setMaximum(3000)
        self.display_height = QSpinBox()
        self.display_height.setMaximum(2400)
        self.extension = QComboBox()
        self.extension.addItems(['.jpg', '.mp4'])
        layout = QVBoxLayout()
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel('Display width (px)'))
        hlayout.addWidget(self.display_width)
        layout.addLayout(hlayout)
        hlayout = QHBoxLayout()
        hlayout.addWidget(QLabel('Display height (px)'))
        hlayout.addWidget(self.display_height)
        layout.addLayout(hlayout)
        layout.addWidget(self.extension)
        self.setLayout(layout)
        
    def config(self):
        self.__cfg = {
            'width': self.display_width.value(),
            'height': self.display_height.value(),
            'extension': self.extension.currentText()
            }
        return self.__cfg

    def setConfig(self, cfg):
        print(cfg)
        w = cfg['width'] if 'width' in cfg and cfg['width'] is not None else 400
        h = cfg['height'] if 'height' in cfg and cfg['width'] is not None  else 300
        ext = cfg['extension'] if 'extension' in cfg and cfg['extension'] is not None else '.jpg'
        self.display_width.setValue(w)
        self.display_height.setValue(h)
        self.extension.setCurrentText(ext)
        self.__cfg = cfg

class PictureWidgetFactory(QgsEditorWidgetFactory):

    def __init__(self):
        super().__init__('Picture')

    def create(self, vl, fieldIdx, editor, parent):
        return PictureWidgetWrapper(vl, fieldIdx, editor, parent)

    def configWidget(self, vl, fieldIdx, parent):
        return PictureWidgetConfig(vl, fieldIdx, parent)

class PictureFieldPlugin():

    def initGui(self):
        QgsGui.editorWidgetRegistry().registerWidget("Picture", PictureWidgetFactory())

    def unload(self):
        pass

def classFactory(iface):
    return PictureFieldPlugin()
