# QGIS Picture Fields

Adds pictures and video display fields in QGIS forms

The plugin uses MS Windows Camera application to take pictures and videos. 

You may need to install K-Lite_Codec_Pack_1540_Basic to read video.

![config](config.png)

![form](form.png)

# Packaging

```
zip -r qgis_picture_field.zip qgis_picture_field
```
